package dev.skripsi.barcodesepatu.utils;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by XGibar on 27/10/2016.
 */
public interface Interface {
    @GET("https://api.barcodespider.com/v1/search?token=f96aaa6d986d303c86cb&s=nike")
    Call<ResponseBody> getDataSepatu();

    @GET("https://api.barcodespider.com/v1/lookup?token=f96aaa6d986d303c86cb&")
    Call<ResponseBody> getCekSepatu(@Query("upc") String upc);
}


