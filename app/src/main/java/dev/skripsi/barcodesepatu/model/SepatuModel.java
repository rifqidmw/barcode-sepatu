package dev.skripsi.barcodesepatu.model;

public class SepatuModel {
    private String title;
    private String upc;
    private String ean;
    private String category;
    private String brand;
    private String model;
    private String mpn;
    private String manufacturer;
    private String publisher;
    private String asin;
    private String color;
    private String size;
    private String weight;
    private String image;
    private String description;

    public SepatuModel() {
    }

    public SepatuModel(String title, String upc, String ean, String category, String brand, String model, String mpn, String manufacturer, String publisher, String asin, String color, String size, String weight, String image, String description) {
        this.title = title;
        this.upc = upc;
        this.ean = ean;
        this.category = category;
        this.brand = brand;
        this.model = model;
        this.mpn = mpn;
        this.manufacturer = manufacturer;
        this.publisher = publisher;
        this.asin = asin;
        this.color = color;
        this.size = size;
        this.weight = weight;
        this.image = image;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMpn() {
        return mpn;
    }

    public void setMpn(String mpn) {
        this.mpn = mpn;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getAsin() {
        return asin;
    }

    public void setAsin(String asin) {
        this.asin = asin;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
