package dev.skripsi.barcodesepatu.module.ui.list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import dev.skripsi.barcodesepatu.R;
import dev.skripsi.barcodesepatu.adapter.SepatuAdapter;
import dev.skripsi.barcodesepatu.model.SepatuModel;
import dev.skripsi.barcodesepatu.module.MainActivity;
import dev.skripsi.barcodesepatu.utils.Api;
import dev.skripsi.barcodesepatu.utils.ParamReq;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListFragment extends Fragment {

    private ListViewModel listViewModel;
    private SepatuAdapter sepatuAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        listViewModel =
                ViewModelProviders.of(this).get(ListViewModel.class);
        View root = inflater.inflate(R.layout.fragment_list, container, false);

        //inisialisasi layout recyclerview
        RecyclerView recyclerView = root.findViewById(R.id.recycler_sepatu);

        //inisialisasi adapter untuk list sepatu
        sepatuAdapter = new SepatuAdapter(getContext(), Api.sepatuList, "1");
        //setting recyclerview
        final LinearLayoutManager mLayout = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        mLayout.setReverseLayout(false);
        mLayout.setStackFromEnd(false);
        recyclerView.setLayoutManager(mLayout);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(sepatuAdapter);

        //mengecek apakah arraylist untuk data sepatu, jika data kosong maka akan mengambil data sepatu ke barcode spider
        if (Api.sepatuList.isEmpty()){
            getData();
        }

        return root;
    }

    private void getData(){
        Call<ResponseBody> call = ParamReq.reqDataSepatu(getContext()); //request data sepatu ke barcode spider
        Callback<ResponseBody> cBack = new Callback<ResponseBody>() { //listener untuk mendapatkan data sepatu dari barcode spider
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string()); //semua data json
                    JSONObject statusObject = jsonObject.getJSONObject("item_response"); //mengambil json object dengan nama item_response
                    JSONArray dataArray = jsonObject.getJSONArray("Data"); //mengambil json array dengan nama Data
                    if (statusObject.getString("code").equals("200")){ //cek jika status nya 200 maka akan melanjutkan proses
                        if (dataArray.length() > 0){ //cek jika jumlah data melebihi 0
                            for (int i=0; i < dataArray.length(); i++){ //looping data
                                SepatuModel model = new SepatuModel();
                                model.setTitle(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("title"));
                                model.setUpc(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("upc"));
                                model.setEan(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("ean"));
                                model.setCategory(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("category"));
                                model.setBrand(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("brand"));
                                model.setMpn(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("mpn"));
                                model.setManufacturer(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("manufacturer"));
                                model.setPublisher(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("publisher"));
                                model.setAsin(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("asin"));
                                model.setColor(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("color"));
                                model.setSize(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("size"));
                                model.setWeight(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("weight"));
                                model.setImage(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("image"));
                                model.setDescription(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("description"));
                                model.setModel(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("model"));
                                Api.sepatuList.add(model); //menyimpan data sepatu ke arraylist

                            }
                            sepatuAdapter.notifyDataSetChanged(); //listener adapter recyclerview untuk refresh data
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        };
        Api.enqueueWithRetry(getContext(), call, cBack, false, "Loading");
    }

}