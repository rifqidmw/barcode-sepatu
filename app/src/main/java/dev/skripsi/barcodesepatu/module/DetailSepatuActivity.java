package dev.skripsi.barcodesepatu.module;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import dev.skripsi.barcodesepatu.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

public class DetailSepatuActivity extends AppCompatActivity {

    private TextView tvMerk, tvTipe, tvWarna, tvDeskripsi;
    private Button btnScan;
    private ImageView imgAvatar;
    private ProgressBar progressBar;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_sepatu);

        //inisialisasi layout
        toolbar = findViewById(R.id.toolbar);
        tvMerk = findViewById(R.id.tv_isi_merk);
        tvTipe = findViewById(R.id.tv_isi_tipe);
        tvWarna = findViewById(R.id.tv_isi_warna);
        tvDeskripsi = findViewById(R.id.tv_isi_deskripsi);
        imgAvatar = findViewById(R.id.img_avatar);
        progressBar = findViewById(R.id.progressBar);
        btnScan = findViewById(R.id.btn_scan);

        //jika button scan di klik
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                DetailSepatuActivity.this.finish(); //menutup activity ini
            }
        });

        //mengambil data intent
        Intent getIntent = getIntent();
        String avatar = getIntent.getStringExtra("AVATAR");
        String merk = getIntent.getStringExtra("MERK");
        String tipe = getIntent.getStringExtra("TIPE");
        String warna = getIntent.getStringExtra("WARNA");
        String deskripsi = getIntent.getStringExtra("DESKRIPSI");

        //setting value ke layout
        tvMerk.setText(merk);
        tvTipe.setText(tipe);
        tvWarna.setText(warna);
        tvDeskripsi.setText(deskripsi);
        progressBar.setVisibility(View.VISIBLE);

        //load image dari url
        Glide.with(DetailSepatuActivity.this)
                .addDefaultRequestListener(new RequestListener<Object>()
                {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Object> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.VISIBLE);
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(Object resource, Object model, Target<Object> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.INVISIBLE);
                        return false;
                    }
                })
                .load(avatar).into(imgAvatar);

    }
}
