package dev.skripsi.barcodesepatu.module;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import dev.skripsi.barcodesepatu.R;
import dev.skripsi.barcodesepatu.adapter.SepatuAdapter;
import dev.skripsi.barcodesepatu.model.SepatuModel;
import dev.skripsi.barcodesepatu.utils.Api;
import dev.skripsi.barcodesepatu.utils.ParamReq;
import dmax.dialog.SpotsDialog;
import info.androidhive.barcode.BarcodeReader;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.zxing.Result;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class ScanBarcodeActivity extends AppCompatActivity{

    Toolbar toolbar;
    private CodeScanner codeScanner;
    private android.app.AlertDialog spotsDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_barcode);

        spotsDialog = new SpotsDialog.Builder().setContext(ScanBarcodeActivity.this).build();

        toolbar = findViewById(R.id.toolbar);
        CodeScannerView scannerView = findViewById(R.id.scanner_view);
        codeScanner = new CodeScanner(this, scannerView);
        codeScanner.startPreview();
        codeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                ScanBarcodeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getData(result.getText());
//                        Toast.makeText(ScanBarcodeActivity.this, "BARCODE : "+result.getText(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                ScanBarcodeActivity.this.finish();
            }
        });
    }

    private void showDialog(final SepatuModel sepatu){
        View mDialogView = LayoutInflater.from(ScanBarcodeActivity.this).inflate(R.layout.model_dialog, null);
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(ScanBarcodeActivity.this).setView(mDialogView);
        final Dialog dialog = mBuilder.create();
        dialog.setCancelable(true);

        dialog.show();

        TextView tvCancel, tvOk;
        final EditText etModel;
        tvCancel = mDialogView.findViewById(R.id.tv_cancel);
        tvOk = mDialogView.findViewById(R.id.tv_ok);
        etModel = mDialogView.findViewById(R.id.et_model);

        Log.e("MODEl", sepatu.getModel());

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                codeScanner.startPreview();
            }
        });

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etModel.getText().toString().equals(sepatu.getModel())){
                    Intent intent = new Intent(ScanBarcodeActivity.this, DetailSepatuActivity.class);
                    intent.putExtra("AVATAR", sepatu.getImage());
                    intent.putExtra("MERK", sepatu.getTitle());
                    intent.putExtra("TIPE", sepatu.getBrand());
                    intent.putExtra("WARNA", sepatu.getColor());
                    intent.putExtra("DESKRIPSI", sepatu.getDescription());
                    startActivity(intent);
                }else {
                    Toast.makeText(ScanBarcodeActivity.this, "Kode tidak sesuai", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();

//                spotsDialog.show();
            }
        });
    }

    private void getData(final String qr){
        Call<ResponseBody> call = ParamReq.reqCekSepatu(ScanBarcodeActivity.this, qr);
        Callback<ResponseBody> cBack = new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    spotsDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject statusObject = jsonObject.getJSONObject("item_response");
                    JSONObject attributeObject = jsonObject.getJSONObject("item_attributes");
                    JSONArray dataArray = jsonObject.getJSONArray("Data");
                    if (statusObject.getString("code").equals("200")){
                        Log.e("UPC123", "UPC: "+attributeObject.getString("title"));
                        /*if (dataArray.length() > 0){
                            if (dataArray.toString().contains(qr)){
                                for (int i=0; i < dataArray.length(); i++){
                                    if (dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("upc").equals(qr)){
                                        SepatuModel model = new SepatuModel();
                                        model.setTitle(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("title"));
                                        model.setUpc(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("upc"));
                                        model.setEan(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("ean"));
                                        model.setCategory(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("category"));
                                        model.setBrand(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("brand"));
                                        model.setMpn(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("mpn"));
                                        model.setManufacturer(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("manufacturer"));
                                        model.setPublisher(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("publisher"));
                                        model.setAsin(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("asin"));
                                        model.setColor(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("color"));
                                        model.setSize(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("size"));
                                        model.setWeight(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("weight"));
                                        model.setImage(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("image"));
                                        model.setDescription(dataArray.getJSONObject(i).getJSONObject("item_attributes").getString("description"));

                                        showDialog(model);
                                    }
                                }
                            } else {
                                Toast.makeText(ScanBarcodeActivity.this, "QR Code tidak terdaftar", Toast.LENGTH_SHORT).show();
                            }
                        }*/
                    }
                } catch (Exception e) {
                    Toast.makeText(ScanBarcodeActivity.this, "Exceed daily request limits", Toast.LENGTH_SHORT).show();
                    codeScanner.startPreview();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        };
        Api.enqueueWithRetry(ScanBarcodeActivity.this, call, cBack, false, "Loading");
    }
}
