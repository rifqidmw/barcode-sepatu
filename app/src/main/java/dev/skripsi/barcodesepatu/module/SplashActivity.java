package dev.skripsi.barcodesepatu.module;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dev.skripsi.barcodesepatu.R;

public class SplashActivity extends AppCompatActivity {


    private static final int PERMISSION_REQUEST_CODE = 1100;
    String[] appPermission = {
            Manifest.permission.CAMERA,
            Manifest.permission.INTERNET,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //check permission camera, internet & storage
                if (checkAndRequestPermission()){
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                }
            }
        }, 500);
    }

    //check & request permission
    public boolean checkAndRequestPermission(){
        List<String> listPermissionNeeded = new ArrayList<>();
        //looping untuk menambahkan data ke arraylist listPermissionNeeded dari string array appPermission
        for (String perm: appPermission){
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionNeeded.add(perm);
            }
        }
        //jika data di listPermissionNeeded tidak kosong maka akan request permission
        if (!listPermissionNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionNeeded.toArray(new String[listPermissionNeeded.size()]), PERMISSION_REQUEST_CODE);
            return false;
        }

        return true;
    }

    //proses untuk pengecekan apakah permission di accept atau tidak
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE){
            //variabel untuk menyimpan nama permission yg ditolak
            HashMap<String, Integer> permissionResults = new HashMap<>();
            //variabel untuk mengetahui jumlah permission yg ditolal
            int deniedCount = 0;

            //looping untuk mengecek permission
            for (int i=0; i<grantResults.length;i++){
                //jika permission ditolak
                if (grantResults[i] == PackageManager.PERMISSION_DENIED){
                    permissionResults.put(permissions[i], grantResults[i]); //menambahkan nama permission ke permissionResult
                    deniedCount++; //menambahkan value jumlah ke deniedCont
                }
                //jika permission di terima maka tidak ada proses lanjutan
            }

            if (deniedCount == 0){ //jika jumlah deniedCount yg berarti sudah tidak ada permission yg ditolak maka tidak akan terjadi apa apa
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
            } else { //jika jumlah deniedCount lebih dari 0 yg berarti masih ada permission yg ditolak maka akan melanjutkan proses yaitu menampilkan dialog untuk granted permission
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()){
                    String permName = entry.getKey();
                    int permResult = entry.getValue();

                    //menampilkan dialog
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)){
                        AlertDialog.Builder dialog= new AlertDialog.Builder(this);
                        dialog.setTitle("Alert");
                        dialog.setMessage("This App need Camera Permission to work without and problems");
                        dialog.setPositiveButton("YES, Granted permission", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) { //jika klik tombol yes di dialog
                                dialog.dismiss(); //menutup dialog
                                checkAndRequestPermission(); //mengecek permission lagi
                            }
                        });
                        dialog.setNegativeButton("No, Exit App", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) { //jika klik tombol no di dialog
                                dialog.dismiss(); //menutup dialog
                                finish(); //menutup activity
                                System.exit(0); //keluar aplikasi
                            }
                        });
                        dialog.setCancelable(false);
                        AlertDialog alert = dialog.create();
                        alert.show();

                    }
                }
            }
        }
    }
}