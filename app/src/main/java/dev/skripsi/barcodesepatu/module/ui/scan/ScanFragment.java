package dev.skripsi.barcodesepatu.module.ui.scan;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import dev.skripsi.barcodesepatu.R;
import dev.skripsi.barcodesepatu.model.SepatuModel;
import dev.skripsi.barcodesepatu.module.DetailSepatuActivity;
import dev.skripsi.barcodesepatu.module.ScanBarcodeActivity;
import dev.skripsi.barcodesepatu.utils.Api;
import dev.skripsi.barcodesepatu.utils.ParamReq;
import dmax.dialog.SpotsDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScanFragment extends Fragment {

    private ScanViewModel scanViewModel;
    Toolbar toolbar;
    private CodeScanner codeScanner;
    private android.app.AlertDialog spotsDialog;
    private Dialog dialog;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        scanViewModel =
                ViewModelProviders.of(this).get(ScanViewModel.class);
        View root = inflater.inflate(R.layout.fragment_scan, container, false);

        //inisialisasi loading dialog
        spotsDialog = new SpotsDialog.Builder().setContext(getActivity()).build();
        //inisialisasi toolbar
        toolbar = root.findViewById(R.id.toolbar);
        //inisialisasi scanner view
        CodeScannerView scannerView = root.findViewById(R.id.scanner_view);
        //inisialisasi code scanner
        codeScanner = new CodeScanner(getActivity(), scannerView);
        codeScanner.startPreview(); //memulai scan
        codeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) { //fungsi dari codeScanner untuk mendapatkan data dari hasil scan
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getData(result.getText()); //untuk mendapatkan data dari scan yaitu dengan perintah result.getText(), setelah mendapatkan data dari hasil scan kemudian melanjutkan proses untuk pencocokan data ke server apakah upc code hasil scan cocok dengan yg ada di server
                    }
                });
            }
        });
        

        return root;
    }

    private void showDialog(final SepatuModel sepatu){
        //setting dialog
        spotsDialog.dismiss();
        View mDialogView = LayoutInflater.from(getContext()).inflate(R.layout.model_dialog, null);
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext()).setView(mDialogView);
        dialog = mBuilder.create();
        dialog.setCancelable(true);

        dialog.show(); //menampilkan dialog

        //inisialisasi layout di dialog
        TextView tvCancel, tvOk;
        final EditText etModel;
        tvCancel = mDialogView.findViewById(R.id.tv_cancel);
        tvOk = mDialogView.findViewById(R.id.tv_ok);
        etModel = mDialogView.findViewById(R.id.et_model); //edittext untuk mengisi data model

        //jika button cancel di klik
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss(); //menutup dialog
                codeScanner.startPreview(); //memulai kembali scan barcode
            }
        });


        //jika button ok di klik
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etModel.getText().toString().equals(sepatu.getModel())){ //mengecek apakah data model yg diisi user sama data model yg dari server
                    Intent intent = new Intent(getContext(), DetailSepatuActivity.class); //menuju ke detail activity
                    intent.putExtra("AVATAR", sepatu.getImage());
                    intent.putExtra("MERK", sepatu.getTitle());
                    intent.putExtra("TIPE", sepatu.getBrand());
                    intent.putExtra("WARNA", sepatu.getColor());
                    intent.putExtra("DESKRIPSI", sepatu.getDescription());
                    startActivity(intent);
                }else { //jika data model yg diisi user tidak sama dengan data model yg dari server
                    Toast.makeText(getContext(), "Model tidak sesuai, silahkan isi ulang nomor model!!!", Toast.LENGTH_SHORT).show(); //menampilkan peringatan bahwa model tidak sama
                }
            }
        });
    }

    private void getData(final String qr){
        if (Api.sepatuList.isEmpty()){ //cek jika arraylist untuk sepatu list kosong maka akan mencocokan data langsung dari server
            spotsDialog.show();
            Call<ResponseBody> call = ParamReq.reqCekSepatu(getContext(), qr); //request data sepatu ke barcode spider
            Callback<ResponseBody> cBack = new Callback<ResponseBody>() { //listener untuk mengambil data sepatu dari barcode spider
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        spotsDialog.dismiss(); //menutup loading dialog
                        JSONObject jsonObject = new JSONObject(response.body().string()); //mengambil semua data json
                        JSONObject statusObject = jsonObject.getJSONObject("item_response"); //mengambil json object dengan nama item_response
                        JSONObject attributeObject = jsonObject.getJSONObject("item_attributes");
                        if (statusObject.getString("code").equals("200")){ //jika json object dengan nama code memiliki value 200 yg berarti berhasil
                            if (attributeObject.getString("brand").equals("Nike")){
                                SepatuModel model = new SepatuModel();
                                model.setTitle(attributeObject.getString("title"));
                                model.setUpc(attributeObject.getString("upc"));
                                model.setEan(attributeObject.getString("ean"));
                                model.setCategory(attributeObject.getString("category"));
                                model.setBrand(attributeObject.getString("brand"));
                                model.setMpn(attributeObject.getString("mpn"));
                                model.setManufacturer(attributeObject.getString("manufacturer"));
                                model.setPublisher(attributeObject.getString("publisher"));
                                model.setAsin(attributeObject.getString("asin"));
                                model.setColor(attributeObject.getString("color"));
                                model.setSize(attributeObject.getString("size"));
                                model.setWeight(attributeObject.getString("weight"));
                                model.setImage(attributeObject.getString("image"));
                                model.setDescription(attributeObject.getString("description"));
                                model.setModel(attributeObject.getString("model"));

                                showDialog(model); //menampilkan dialog pengecekan model
                            } else {
                                Toast.makeText(getContext(), "QR Code tidak terdaftar", Toast.LENGTH_SHORT).show();
                                codeScanner.startPreview();
                            }

                        } else {
                            Toast.makeText(getContext(), "QR Code tidak terdaftar", Toast.LENGTH_SHORT).show();
                            codeScanner.startPreview();
                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    codeScanner.startPreview();
                }
            };
            Api.enqueueWithRetry(getContext(), call, cBack, false, "Loading");
        } else { //jika arraylist sepatu tidak kosong maka pengecekan akan langsung tanpa mengambil data dari barcode spiner
            ArrayList<String> upcList = new ArrayList<>();
            for (int i = 0; i < Api.sepatuList.size(); i++){ //looping untuk mengambil data upc dari arraylist sepatu
                upcList.add(Api.sepatuList.get(i).getUpc()); //kemudian data upc dimasukkan ke arraylist upcList
            }

            if (upcList.contains(qr)){
                for (int i = 0; i < upcList.size(); i++){ //looping untuk upcList
                    if (qr.equals(upcList.get(i))){ //cek jika data qr sesuai dengan salah satu data dari upcList
                        showDialog(Api.sepatuList.get(i)); //menampilkan dialog pengecekan model
                    }
                }
            } else {
                Toast.makeText(getContext(), "QR Code tidak terdaftar", Toast.LENGTH_SHORT).show(); //menampilkan peringatan bahwa qr code tidak terdaftar
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        codeScanner.startPreview();
    }
}