package dev.skripsi.barcodesepatu.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import dev.skripsi.barcodesepatu.R;
import dev.skripsi.barcodesepatu.model.SepatuModel;
import dev.skripsi.barcodesepatu.module.DetailSepatuActivity;

public class SepatuAdapter extends RecyclerView.Adapter<SepatuAdapter.MyViewHolder> {
    private Context mContext;
    private List<SepatuModel> sepatuList;
    private View itemView;
    private String status;
    public static ProgressDialog mProgressDialog;


    public SepatuAdapter(Context mContext, List<SepatuModel> sepatuList, String status) {
        this.mContext = mContext;
        this.sepatuList = sepatuList;
        this.status = status;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sepatu, parent, false);

        return new SepatuAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final SepatuModel sepatu = sepatuList.get(position);
        //setting data value ke layout
        holder.tvTitle.setText(sepatu.getTitle());
        holder.tvBrand.setText(sepatu.getBrand());
        holder.tvUpc.setText(sepatu.getUpc());
    }

    @Override
    public int getItemCount() {
        return sepatuList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle, tvBrand, tvUpc;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            //inisialisasi layout
            tvTitle = itemView.findViewById(R.id.text_title);
            tvBrand = itemView.findViewById(R.id.text_brand);
            tvUpc = itemView.findViewById(R.id.text_upc);
        }
    }

    public void setFilter(List<SepatuModel> newList){
        sepatuList=new ArrayList<>();
        sepatuList.addAll(newList);
        notifyDataSetChanged();
    }
}
